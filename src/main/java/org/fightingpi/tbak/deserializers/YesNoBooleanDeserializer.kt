package org.fightingpi.tbak.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer

class YesNoBooleanDeserializer: JsonDeserializer<Boolean>() {
    override fun deserialize(parser: JsonParser?, ctx: DeserializationContext?): Boolean {
        val token = parser?.currentToken ?: return false
        return token == JsonToken.VALUE_STRING && parser.text.equals("yes", true)
    }
}
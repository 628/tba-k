package org.fightingpi.tbak

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.util.StdDateFormat
import io.ktor.client.*
import io.ktor.client.engine.java.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import org.fightingpi.tbak.model.Media
import org.fightingpi.tbak.model.api.APIStatus
import org.fightingpi.tbak.model.district.DistrictRanking
import org.fightingpi.tbak.model.event.Event
import org.fightingpi.tbak.model.event.EventSimple
import org.fightingpi.tbak.model.match.Match
import org.fightingpi.tbak.model.match.MatchSimple
import org.fightingpi.tbak.model.team.Team
import org.fightingpi.tbak.model.team.TeamEventStatus
import org.fightingpi.tbak.model.team.TeamSimple

class TBAK(private val tbaAuthKey: String) {
    private val client = HttpClient(Java) {
        defaultRequest {
            header("X-TBA-Auth-Key", tbaAuthKey)
        }

        install(JsonFeature) {
            serializer = JacksonSerializer {
                enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
                propertyNamingStrategy = PropertyNamingStrategy.SNAKE_CASE
                configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                dateFormat = StdDateFormat().withColonInTimeZone(false)
            }
        }
    }

    suspend fun getStatus(): APIStatus = client.get("https://www.thebluealliance.com/api/v3/status")

    suspend fun getTeams(page: Int): List<Team> = client.get("https://www.thebluealliance.com/api/v3/teams/$page")

    suspend fun getTeamsSimple(page: Int): List<TeamSimple> =
        client.get("https://www.thebluealliance.com/api/v3/teams/$page/simple")

    suspend fun getTeamKeys(page: Int): List<String> =
        client.get("https://www.thebluealliance.com/api/v3/teams/$page/keys")

    suspend fun getTeamsInYear(year: Int, page: Int): List<Team> =
        client.get("https://www.thebluealliance.com/api/v3/teams/$year/$page")

    suspend fun getTeamsInYearSimple(year: Int, page: Int): List<TeamSimple> =
        client.get("https://www.thebluealliance.com/api/v3/teams/$year/$page/simple")

    suspend fun getTeamInYearKeys(year: Int, page: Int): List<String> =
        client.get("https://www.thebluealliance.com/api/v3/teams/$year/$page/keys")

    suspend fun getTeamEventStatuses(team: Int, year: Int): Map<String, TeamEventStatus> =
        client.get("https://www.thebluealliance.com/api/v3/team/frc$team/events/$year/statuses")

    suspend fun getEventsInYear(year: Int): List<Event> =
        client.get("https://www.thebluealliance.com/api/v3/events/$year")

    suspend fun getEventsInYearSimple(year: Int): List<EventSimple> =
        client.get("https://www.thebluealliance.com/api/v3/events/$year/simple")

    suspend fun getEventInYearKeys(year: Int): List<String> =
        client.get("https://www.thebluealliance.com/api/v3/events/$year/keys")

    suspend fun getTeamsAtEvent(year: Int, eventCode: String): List<Team> =
        client.get("https://www.thebluealliance.com/api/v3/event/$year$eventCode/teams")

    suspend fun getTeamsAtEventSimple(year: Int, eventCode: String): List<TeamSimple> =
        client.get("https://www.thebluealliance.com/api/v3/event/$year$eventCode/teams/simple")

    suspend fun getTeamAtEventKeys(year: Int, eventCode: String): List<String> =
        client.get("https://www.thebluealliance.com/api/v3/event/$year$eventCode/teams/keys")

    suspend fun getDistrictRankings(year: Int, districtCode: String): List<DistrictRanking> =
        client.get("https://www.thebluealliance.com/api/v3/district/$year$districtCode/rankings")

    suspend fun getTeamEvents(team: Int): List<Event> = client.get("https://www.thebluealliance.com/api/v3/team/frc$team/events")

    suspend fun getTeamEventsSimple(team: Int): List<EventSimple> = client.get("https://www.thebluealliance.com/api/v3/team/frc$team/events/simple")

    suspend fun getTeamEventKeys(team: Int): List<String> = client.get("https://www.thebluealliance.com/api/v3/team/frc$team/events/keys")

    suspend fun getSocialMedia(team: Int): List<Media> = client.get("https://www.thebluealliance.com/api/v3/team/frc$team/social_media")

    suspend fun getMatches(team: Int, eventCode: String, year: Int): List<Match> = client.get("https://www.thebluealliance.com/api/v3/team/frc$team/event/$year$eventCode/matches")

    suspend fun getMatchesSimple(team: Int, eventCode: String, year: Int): List<MatchSimple> = client.get("https://www.thebluealliance.com/api/v3/team/frc$team/event/$year$eventCode/matches/simple")

    suspend fun getMatchKeys(team: Int, eventCode: String, year: Int): List<String> = client.get("https://www.thebluealliance.com/api/v3/team/frc$team/event/$year$eventCode/matches/keys")

    suspend fun getMatch(matchKey: String): Match = client.get("https://www.thebluealliance.com/api/v3/match/$matchKey")

    suspend fun getMatchSimple(matchKey: String): MatchSimple = client.get("https://www.thebluealliance.com/api/v3/match/$matchKey")
}
package org.fightingpi.tbak.types

enum class Season(val year: Int) {
    RAPID_REACT(2022);

    override fun toString(): String {
        return year.toString()
    }
}

enum class MatchLevel { None, Practice, Qualification, Playoff }

enum class Alliance { Red, Blue }

enum class RRHangarBar { None, Low, Mid, High, Traversal }


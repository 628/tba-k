package org.fightingpi.tbak.model.event

data class EventOPRs(
    val oprs: Map<String, Double>?,
    val dprs: Map<String, Double>?,
    val ccws: Map<String, Double>?
)

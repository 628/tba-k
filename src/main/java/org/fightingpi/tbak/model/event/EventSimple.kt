package org.fightingpi.tbak.model.event

import com.fasterxml.jackson.annotation.JsonFormat
import org.fightingpi.tbak.model.district.DistrictList
import java.util.Date

data class EventSimple(
    val key: String,
    val name: String,
    val eventCode: String,
    val eventType: Int,
    val district: DistrictList?,
    val city: String?,
    val stateProv: String?,
    val country: String?,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    val startDate: Date,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    val endDate: Date,
    val year: Int
)

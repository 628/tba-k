package org.fightingpi.tbak.model.event

data class EventPoints(
    val total: Int,
    val alliancePoints: Int,
    val elimPoints: Int,
    val awardPoints: Int,
    val qualPoints: Int,
)

data class TiebreakerValues(
    val highestQualScores: List<Int>,
    val qualWins: Int
)

data class EventDistrictPoints(
    val points: Map<String, EventPoints>,
    val tiebreakers: Map<String, TiebreakerValues>?
)

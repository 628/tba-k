package org.fightingpi.tbak.model.event

import org.fightingpi.tbak.model.team.SortOrderInfo
import org.fightingpi.tbak.model.team.TeamRanking

data class EventRanking(
    val extraStatsInfo: List<SortOrderInfo>?,
    val rankings: TeamRanking,
    val sortOrderInfo: List<SortOrderInfo>
)

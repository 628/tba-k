package org.fightingpi.tbak.model.event

import com.fasterxml.jackson.annotation.JsonFormat
import org.fightingpi.tbak.model.Webcast
import org.fightingpi.tbak.model.district.DistrictList
import java.util.Date

data class Event(
    val key: String,
    val name: String,
    val eventCode: String,
    val eventType: Int,
    val district: DistrictList?,
    val city: String?,
    val stateProv: String?,
    val country: String?,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    val startDate: Date,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    val endDate: Date,
    val year: Int,
    val shortName: String?,
    val eventTypeString: String,
    val week: Int?,
    val address: String?,
    val postalCode: String?,
    val gmapsPlaceId: String?,
    val gmapsUrl: String?,
    val lat: Double?,
    val lng: Double?,
    val locationName: String?,
    val timezone: String?,
    val website: String?,
    val firstEventId: String?,
    val firstEventCode: String?,
    val webcasts: List<Webcast>?,
    val divisionKeys: List<String>?,
    val parentEventKey: String?,
    val playoffType: Int?,
    val playoffTypeString: String?
)

package org.fightingpi.tbak.model

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.fightingpi.tbak.deserializers.YesNoBooleanDeserializer
import org.fightingpi.tbak.model.types.RREndgame

data class RRScoreBreakdownAlliance(
    @JsonDeserialize(using = YesNoBooleanDeserializer::class)
    val taxiRobot1: Boolean,
    val endgameRobot1: RREndgame,
    @JsonDeserialize(using = YesNoBooleanDeserializer::class)
    val taxiRobot2: Boolean,
    val endgameRobot2: RREndgame,
    val taxiRobot3: Boolean,
    val endgameRobot3: RREndgame,
    val autoCargoLowerNear: Int,
    val autoCargoLowerFar: Int,
    val autoCargoLowerBlue: Int,
    val autoCargoLowerRed: Int,
    val autoCargoUpperNear: Int,
    val autoCargoUpperFar: Int,
    val autoCargoUpperBlue: Int,
    val autoCargoUpperRed: Int,
    val autoCargoTotal: Int,
    val teleopCargoLowerNear: Int,
    val teleopCargoLowerFar: Int,
    val teleopCargoLowerBlue: Int,
    val teleopCargoLowerRed: Int,
    val teleopCargoUpperNear: Int,
    val teleopCargoUpperFar: Int,
    val teleopCargoUpperBlue: Int,
    val teleopCargoUpperRed: Int,
    val teleopCargoTotal: Int,
    val matchCargoTotal: Int,
    val autoTaxiPoints: Int,
    val autoCargoPoints: Int,
    val autoPoints: Int,
    val quintetAchieved: Boolean,
    val teleopCargoPoints: Int,
    val endgamePoints: Int,
    val cargoBonusRankingPoint: Boolean,
    val hangarBonusRankingPoint: Boolean,
    val foulCount: Int,
    val techFoulCount: Int,
    val adjustPoints: Int,
    val foulPoints: Int,
    val rp: Int,
    val totalPoints: Int
)

data class RRScoreBreakdown(
    val blue: RRScoreBreakdownAlliance,
    val red: RRScoreBreakdownAlliance
)

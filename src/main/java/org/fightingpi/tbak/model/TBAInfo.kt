package org.fightingpi.tbak.model

data class TBAInfo(
    val name: String,
    val precision: Int
)
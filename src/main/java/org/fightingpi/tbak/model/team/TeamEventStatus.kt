package org.fightingpi.tbak.model.team

import com.fasterxml.jackson.annotation.JsonProperty
import org.fightingpi.tbak.model.WLTRecord
import org.fightingpi.tbak.model.types.PlayoffStatus

data class SortOrderInfo(
    val precision: String?,
    val name: String?
)

data class TeamEventStatusRank(
    val numTeams: Int?,
    val ranking: TeamRanking?,
    val sortOrderInfo: List<SortOrderInfo>?,
    val status: String?
)

data class TeamAllianceBackup(
    val description: String?,
    val out: String?,
    @JsonProperty("in")
    val calledIn: String?
)

data class TeamEventStatusAlliance(
    val name: String?,
    val number: Int?,
    val backup: TeamAllianceBackup?,
    val pick: Int?
)

data class TeamEventStatusPlayoff(
    val description: String?,
    val level: String?,
    val currentLevelRecord: WLTRecord?,
    val record: WLTRecord?,
    val status: PlayoffStatus?,
    val playoffAverage: Int?
)

data class TeamEventStatus(
    val qual: TeamEventStatusRank?,
    val alliance: TeamEventStatusAlliance?,
    val playoff: TeamEventStatusPlayoff?,
    val allianceStatusStr: String?,
    val playoffStatusStr: String?,
    val overallStatusStr: String?,
    val nextMatchKey: String?,
    val lastMatchKey: String?
)
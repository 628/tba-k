package org.fightingpi.tbak.model.team

data class TeamSimple(
    val key: String,
    val teamNumber: Int,
    val nickname: String?,
    val name: String,
    val city: String?,
    val stateProv: String?,
    val country: String?
)

package org.fightingpi.tbak.model.team

data class Team(
    val key: String,
    val teamNumber: Int,
    val nickname: String?,
    val name: String,
    val schoolName: String?,
    val city: String?,
    val stateProv: String?,
    val country: String?,
    val website: String?,
    val rookieYear: Int?,
    val motto: String?,
    val homeChampionship: Map<String, String>?
)

package org.fightingpi.tbak.model.team

import org.fightingpi.tbak.model.WLTRecord

data class TeamRanking(
    val matchesPlayed: Int,
    val qualAverage: Double,
    val sortOrders: List<Double>,
    val record: WLTRecord,
    val rank: Int,
    val dq: Int,
    val teamKey: String
)

package org.fightingpi.tbak.model.team

data class TeamRobot(
    val year: Int,
    val robotName: String,
    val key: String,
    val teamKey: String
)

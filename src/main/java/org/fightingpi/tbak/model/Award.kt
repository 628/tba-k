package org.fightingpi.tbak.model

data class AwardRecipient(
    val teamKey: String?,
    val awardee: String?
)

data class Award(
    val name: String,
    val awardType: Int,
    val eventKey: String,
    val recipientList: List<AwardRecipient>,
    val year: Int
)

package org.fightingpi.tbak.model

data class Media(
    val type: String,
    val foreignKey: String,
    val preferred: Boolean?,
    val directUrl: String?,
    val viewUrl: String?
)

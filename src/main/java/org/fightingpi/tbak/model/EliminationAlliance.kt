package org.fightingpi.tbak.model

import org.fightingpi.tbak.model.team.TeamAllianceBackup
import org.fightingpi.tbak.model.team.TeamEventStatus

data class EliminationAlliance(
    val name: String?,
    val backup: TeamAllianceBackup?,
    val declines: List<String>?,
    val picks: List<String>,
    val status: TeamEventStatus?
)

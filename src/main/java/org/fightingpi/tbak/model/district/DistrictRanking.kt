package org.fightingpi.tbak.model.district

import org.fightingpi.tbak.model.event.EventDistrictPoints

data class DistrictRanking(
    val teamKey: String,
    val rank: Int,
    val rookieBonus: Int?,
    val pointTotal: Int,
    val eventPoints: List<EventDistrictPoints>
)
package org.fightingpi.tbak.model.district

data class DistrictList(
    val abbreviation: String,
    val displayName: String,
    val key: String,
    val year: Int
)

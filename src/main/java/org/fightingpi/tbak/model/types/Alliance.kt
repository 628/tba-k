package org.fightingpi.tbak.model.types

enum class Alliance {
    RED, BLUE
}
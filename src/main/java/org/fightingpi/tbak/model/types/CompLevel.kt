package org.fightingpi.tbak.model.types

enum class CompLevel(val shortName: String, val longName: String) {
    qm("Qual", "Qualification"),
    ef("Eighth", "Eighth Final"),
    qf("Quarter", "Quarter Final"),
    sf("Semi", "Semi Final"),
    f("Final", "Final")
}
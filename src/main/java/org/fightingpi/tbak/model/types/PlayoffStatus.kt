package org.fightingpi.tbak.model.types

enum class PlayoffStatus {
    WON,
    ELIMINATED,
    PLAYING
}
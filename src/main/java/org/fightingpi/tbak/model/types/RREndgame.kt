package org.fightingpi.tbak.model.types

enum class RREndgame {
    NONE, LOW, MID, HIGH, TRAVERSAL
}
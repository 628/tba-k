package org.fightingpi.tbak.model.api

data class APIStatus(
    val currentSeason: Int,
    val maxSeason: Int,
    val isDatafeedDown: Boolean,
    val downEvents: List<String>,
    val ios: APIStatusAppVersion,
    val android: APIStatusAppVersion
)

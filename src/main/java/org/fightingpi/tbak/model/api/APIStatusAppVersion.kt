package org.fightingpi.tbak.model.api

data class APIStatusAppVersion(
    val minAppVersion: Int,
    val latestAppVersion: Int
)

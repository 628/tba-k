package org.fightingpi.tbak.model

import com.fasterxml.jackson.annotation.JsonFormat

data class Webcast(
    val type: String,
    val channel: String,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    val date: String?,
    val file: String?
)

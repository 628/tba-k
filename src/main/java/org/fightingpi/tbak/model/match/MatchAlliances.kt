package org.fightingpi.tbak.model.match

data class MatchAlliances(
    val red: MatchAlliance,
    val blue: MatchAlliance
)

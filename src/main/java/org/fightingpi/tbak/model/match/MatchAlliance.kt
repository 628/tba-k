package org.fightingpi.tbak.model.match

data class MatchAlliance(
    val score: Int,
    val teamKeys: List<String>,
    val surrogateTeamKeys: List<String>?,
    val dqTeamKeys: List<String>?
)

package org.fightingpi.tbak.model.match

import org.fightingpi.tbak.model.types.CompLevel

data class MatchSimple(
    val key: String,
    val compLevel: CompLevel,
    val setNumber: Int,
    val matchNumber: Int,
    val alliances: MatchAlliances?,
    val winningAlliance: String?,
    val eventKey: String,
    val time: Long?,
    val predictedTime: Long?,
    val actualTime: Long?
)

package org.fightingpi.tbak.model.match

import org.fightingpi.tbak.model.Video
import org.fightingpi.tbak.model.types.CompLevel

data class Match(
    val key: String,
    val compLevel: CompLevel,
    val setNumber: Int,
    val matchNumber: Int,
    val alliances: MatchAlliances?,
    val winningAlliance: String?,
    val eventKey: String,
    val time: Long?,
    val actualTime: Long?,
    val predictedTime: Long?,
    val postResultTime: Long?,
    val videos: List<Video>
)

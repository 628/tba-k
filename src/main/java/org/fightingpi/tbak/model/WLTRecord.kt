package org.fightingpi.tbak.model

data class WLTRecord(
    val losses: Int,
    val wins: Int,
    val ties: Int
)
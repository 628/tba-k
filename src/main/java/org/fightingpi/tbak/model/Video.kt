package org.fightingpi.tbak.model

data class Video(
    val type: String,
    val key: String
)
